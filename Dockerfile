From ubuntu:16.04
MAINTAINER Callum Dryden <callumdryden@gmail.com>

RUN apt-get update \
 && DEBCONF_FRONTEND=noninteractive apt-get install -y icecast2

EXPOSE 8000

WORKDIR /src
COPY icecast.xml icecast.xml
COPY entrypoint.sh entrypoint.sh

RUN mkdir log
RUN touch log/error.log
RUN touch log/access.log
RUN chmod -R g+rw /src
RUN chmod +x entrypoint.sh

USER icecast2

ENTRYPOINT ["/src/entrypoint.sh"]

CMD ["/usr/bin/icecast2", "-c", "icecast.xml"]
